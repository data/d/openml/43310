# OpenML dataset: Indian-Startup-Funding

https://www.openml.org/d/43310

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Interested in the Indian startup ecosystem just like me Wanted to know what type of startups are getting funded in the last few years Wanted to know who are the important investors Wanted to know the hot fields that get a lot of funding these days 
 This dataset is a chance to explore the Indian start up scene. Deep dive into funding data and derive insights into the future

Content
This dataset has funding information of the Indian startups from January 2015 to August 2017. It includes columns with the date funded, the city the startup is based out of, the names of the funders, and the amount invested (in USD).  
For more information on the values of individual fields, check out the Column Metadata.

Acknowledgements
Thanks to trak.in who are generous enough to share the data publicly for free. 

Inspiration
Possible questions which could be answered are:

How does the funding ecosystem change with time
Do cities play a major role in funding
Which industries are favored by investors for funding
Who are the important investors in the Indian Ecosystem
How much funds does startups generally get in India

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43310) of an [OpenML dataset](https://www.openml.org/d/43310). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43310/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43310/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43310/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

